/**
 * Created by anderson.moraes on 02/05/2016.
 */

'use strict';

var express = require('./config/express');
var server = express();

var port = 3005;
var idx = 1 + process.argv.indexOf('-port');
if (idx > 0 && idx < process.argv.length) {
    var tmp = Math.floor(process.argv[idx]);
    if (tmp) port = tmp;

}

console.log('novo');
server.listen(port);
console.log('Server running at http://localhost:'+port+'/');

