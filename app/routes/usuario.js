/**
 * Created by anderson.moraes on 02/05/2016.
 */


module.exports = function(server) {
	const routePromisse = require('../utils/route_promisse_adapter'),
    	  usuario = server.app.controllers.usuario;

    server.get('/usuario/consultar', routePromisse.resolvePromisse(usuario.consultar));
    server.get('/usuario/consultar/:id', routePromisse.resolvePromisse(usuario.consultarPorId));
    server.post('/usuario/incluir', routePromisse.resolvePromisse(usuario.post));
    server.put('/usuario/alterar/:id', routePromisse.resolvePromisse(usuario.put));
    server.delete('/usuario/excluir/:id', routePromisse.resolvePromisse(usuario.excluir));
};
