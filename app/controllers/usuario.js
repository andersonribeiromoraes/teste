/**
 * Created by anderson.moraes on 02/05/2016.
 */
const Q = require('q'),
      assert = require('assert'),   
      Usuario = require('../models/usuario');

module.exports = {
    consultar: function (req, res) {

        var deferred = Q.defer(),
            filter = {
                attributes: ['id', 'login', 'nome', 'email', 'senha']
            };

        Usuario.findAll(filter)
            .then(function (usuarios) {
                if (usuarios.length == 0){                    
                    deferred.resolve("Nenhum usuário encontrado.");
                }else{                    
                    deferred.resolve(usuarios);
                }
            })
            .catch(function(){                          
                deferred.reject(err);
            });    

        return deferred.promise;    
    },

    consultarPorId: function (req, res) {      

        var deferred = Q.defer(),
            filter = {
                where : {
                    id : req.params.id
                },
                attributes: ['id', 'login', 'nome', 'email', 'senha']
            };

        Usuario.findOne(filter)
            .then(function(usuario) {
                if (usuario.length == 0){                    
                    deferred.resolve("Nenhum usuário encontrado.");
                }else{                    
                    deferred.resolve(usuario);
                }
            }).catch(function(err){                
                deferred.reject(err);
            });

        return defer.promise;    
    },

    post: function (req, res) {

        var deferred = Q.defer(),
            data = JSON.parse(JSON.stringify(req.body));                

        assert(data && data.login, "Parâmrtros Inválidos!");

        Usuario.create({
            'login': data.login,
            'nome': data.nome,
            'email': data.email,
            'senha': data.senha
        })
        .then(function (result) {
            deferred.resolve('Usuário inserido com sucesso.');            
        })
        .catch(function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    },

    put: function (req, res) {

        var deferred = Q.defer(),
            data = JSON.parse(JSON.stringify(req.body));
        
        assert(data && data.login, "Parâmrtros Inválidos!");

        var filter = {
            where : {
                id : req.params.id
            },
            attributes: ['id']
        };

        Usuario.findOne(filter)
            .then(function(usuario) {
                usuario.updateAttributes(data)
                    .then(function (result) {
                        deferred.resolve('Usuário atualizado com sucesso.');                        
                    })
                    .catch(function (err) {
                        deferred.reject(err);
                    });
            }).catch(function(err){
                deferred.reject(err);     
            });    

        return deferred.promise;    
    },

    excluir: function(req, res){

        assert(req.params && req.params.id, "Parâmrtros Inválidos!");

        var deferred = Q.defer(),
            filter = {
                where : {
                    id : req.params.id
                },
                attributes: ['id', 'login']
            };

        Usuario.findOne(filter)
            .then(function(usuario) {

                assert(usuario, "Nenhum usuário encontrado!");

                usuario.destroy()
                    .then(function (result) {                        
                        deferred.resolve('Usuário excluido com sucesso.');
                    })
                    .catch(function (err) {
                        deferred.reject(err);
                    });
            }).catch(function(err){
                deferred.reject(err);     
            });

        return deferred.promise;               
    }
};  