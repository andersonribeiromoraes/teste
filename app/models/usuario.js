/**
 * Created by anderson.moraes on 02/05/2016.
 */

var connection = require('../../config/connection'),
    Sequelize = require('sequelize');

var usuario = connection.sequelize.define('Usuario',{    
    id: { type: Sequelize.INTEGER, primaryKey: true },    
    nome: { type: Sequelize.STRING },
    login: { type: Sequelize.STRING },
    email: { type: Sequelize.STRING },
    senha: { type: Sequelize.STRING }
}, {
    tableName: 'usuario',
    freezeTableName: true,
    timestamps: false
});

module.exports = usuario;
