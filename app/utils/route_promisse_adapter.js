/**
 * Created by anderson.moraes on 05/05/16.
 */

var result = require('./result');
var Response = require('./Response');
var util = require('util');

exports.resolvePromisse = function(controllerPromise) {
    return function(req, res, next) {

        res.useResponse = function(bool, type) {
            if(type != "xml"){
                type = "json";
            }
            if(req.query.format_response == "xml"){
                type = req.query.format_response;
            }

            res.usePatternResponse = bool;
            res.formatOfResponse = type;
        };
        var executedController;

        var buildResponse = function(status, data) {           

            if (res.usePatternResponse) {
                return new Response(req, res, {
                    status: status,
                    data: data
                });
            }

            return res.status(status).send(data);
        };


        try {
            executedController = controllerPromise(req, res)
        } catch (err) {
            console.log(err);            
            return buildResponse(result.InternalError, err);
        }

        executedController
            .then(function(val) {
                console.log(req.method);

                if (typeof val == "object" && val.forceHTTPStatus) {
                    var status = val.forceHTTPStatus;
                    delete val.forceHTTPStatus;
                } else {
                    var status = 200;

                    if (req.method == 'GET')
                        status = result.statusOK;
                    else if (req.method == 'POST')
                        status = result.statusCreated;
                    else if (req.method == 'PUT')
                        status = result.statusAccepted;
                    else if (req.method == 'DELETE')
                        status = result.statusAccepted;
                }

                return buildResponse(status, val);                
            })
            .catch(function(err) {
                if (!res.headersSent) {
                    
                    console.error(err.stack || err);
                    if (err.http_code) {
                        return buildResponse(err.http_code, err.message);                        
                    } else if (err.status) {
                        return buildResponse(err.status, err.message);                        
                    } else {
                        console.log(err);
                        return buildResponse(result.InternalError, (err || new Error("Erro no servidor")));                        
                    }
                }
            })
            .catch(function(err) {                
                console.log(err);                
            })
            .done();
    };
};

