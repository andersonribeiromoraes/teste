var util 	= require('util');
var xml2js  = require('xml2js');

module.exports = (function(){


	function Response(req, res, config){
		this.status 	= 200;
		this.success	= true;
		this.msg		= "";
		this.data		= [];
		this.defaults	= ['success','status','data','msg'];

		this.mergeProperties(res, config);
		this.emit(res);
	};


	Response.prototype.mergeProperties = function(res, config) {
		this.status 	= config.status || res.statusCode || this.status;
		this.success	= this.checkSuccess(res, config);

		if(util.isError(config.data)){
			this.data	= {stackTrace: config.data.stack};
			this.msg	= config.data.message || "";
			this.success= false;
		}else if(typeof config.data === "object"){
			this.data	= config.data || [];
			this.msg	= config.data.msg || "";

			delete config.data.msg;
			delete config.data.success;
		}else if(typeof config.data === "string"){
			this.data	= [];
			this.msg	= config.data || "";
		}

		
	};


	Response.prototype.checkSuccess = function(res, config) {
		if(util.isBoolean(config.success)) {
			return config.success;
		}else if(this.status >= 200 && this.status <= 299){
			return true;
		}else{
			return false;
		}
	};


	Response.prototype.toString = function() {
		return JSON.stringify(this.doDefaults());
	};


	Response.prototype.toJSON = function() {
		return this.doDefaults();
	};


	Response.prototype.doDefaults = function() {
		var obj = {};

		for(var a in this){
			if(this.hasOwnProperty(a) && !!~this.defaults.indexOf(a)){
				obj[a] = this[a];
			}
		}

		return obj;
	};


	Response.prototype.emit = function(res) {
		if(res.formatOfResponse == "xml"){
			var builder = new xml2js.Builder();
			var xml = builder.buildObject(JSON.parse(this.toString()));

			return res.status(this.status).send(xml);
		}

		return res.status(this.status).json(this);
	};


	return Response;

})();