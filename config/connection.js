/**
 * Created by anderson.moraes on 02/05/2016.
 */

const Sequelize = require('sequelize'),
    config = require('../config')
    ;


module.exports = {
    sequelize: new Sequelize(config.sequelize.db, config.sequelize.user, config.sequelize.pass, {
        host: config.sequelize.host,        
        port: config.sequelize.port,
        pool: config.sequelize.pool,

        log: console.log
    })
};