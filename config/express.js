/**
 * Created by anderson.moraes on 02/05/2016.
 */
 
var express = require('express'),
    config = require('./config'),
    bodyParser = require('body-parser'),
    timeout = require('connect-timeout'),
    load = require('express-load')
    ;

module.exports = function() {
    var app = express();
    if(config.usaTimeout){
        app.use(timeout('50s'));
    }

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        next();
    });

    app.use(bodyParser.json({limit: '50mb'}));
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.text({defaultCharset:'utf-8'}));

    load('app/models').then('app/controllers').then('app/routes').into(app);

    return app;
};

