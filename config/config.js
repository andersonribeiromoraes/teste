/**
 * Created by anderson.moraes on 02/05/2016.
 */
 
module.exports = {
    production: {
        sequelize: {            
            host: 'localhost',
            port: '3306',
            db: 'teste',
            pass: '123456',
            user: 'root',            
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        },
        apiURL: 'http://localhost:3000',
    },

    development: {
        sequelize: {            
            host: 'localhost',
            port: '3306',
            db: 'teste',
            pass: '123456',
            user: 'root',            
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        },
        apiURL: 'http://localhost:3000',
    },

    test: {
        sequelize: {            
            host: 'localhost',
            db: 'teste',
            pass: '123456',
            user: 'root',            
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        },
        apiURL: 'http://localhost:3000',
    }
};